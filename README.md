# CSS3 Animated Scroller for accesso#
Demo scroller with different speeds
It has a force reset on the bottom right corner, it will also reset on a particular time set in the ApplicationController

## Requirements ##
  * Node.js <http://nodejs.org/>
## Development Environment Setup ##
  1. Install Node.JS
	Use your system package manager (brew,port,apt-get,yum etc)
  2. Install global Bower, Grunt and PhantomJS commands, this step is not necessary,
  because once you perform grunt build it will install all the necessary files from package.json
	  ``bash
		sudo npm install -g grunt-cli bower phantomjs
	  ``
## Development Work-flow ##
  1. Download necessary libraries from bower.js file
	  bower install
  2. Build Development/Distribution
	  ``bash
	  grunt
	  or
	  grunt build
	  ``
  2. Make changes (reflected live in browser) while in development.
     If you make changes and want to reflect in distribution run step 1
  3. Run unit tests
	 Still trying to learn and practice unit testing during the meantime read Angular JS <http://angularjs.org/>
## Deployment ##
  1. Run build scripts
	  ``bash
	  grunt build
		``
  2. Serve compiled, production-ready files from the 'dist' directory
	  ``bash
	  grunt server:dist
		``













