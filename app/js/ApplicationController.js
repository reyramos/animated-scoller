/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController'
	,[
		'$rootScope'
		, '$http'
		, '$scope'
		, function
			(
				$rootScope
				, $http
				, $scope
				) {

			$rootScope.siteName = 'Accesso Demo'

			$http.get('config/config.json').success(function(data) {
				$scope.scroller = data;

				$scope.restart(false)

			});


			function queuingCounter(speed, reset){
				/*
				 Queuing:
				 4,200,000,000
				 Increase 1 at .0004 per second
				 */
				$('#queuing').counter({
						  initial: $scope.scroller.queuing.start,
						  direction: 'up',
						  interval: speed,
						  format: '9,999,999,999',
						  forceReset:reset,
						  reset: $scope.scroller.resetTime,
						  stop: $scope.scroller.queuing.stop
					  });
			}

			function ticketSoldCounter(speed, reset){
				/*
				 Ticketing:
				 77,700,000
				 Increase at 1 per .5 seconds
				 */
				$('#ticketing').counter({
						initial: $scope.scroller.ticketing.start,
						direction: 'up',
						interval: speed,
						format: '9,999,999,999',
						forceReset:reset,
						reset: $scope.scroller.resetTime,
						stop: $scope.scroller.ticketing.stop
					});
			}

			function mobileAppsCounter(speed, reset){
				/*
				 Apps:
				 2,500,000
				 Increase at 1 per 10 seconds
				 */

				$('#apps').counter({
						initial: $scope.scroller.apps.start,
						direction: 'up',
						interval: speed,
						format: '9,999,999,999',
						forceReset:reset,
						reset: $scope.scroller.resetTime,
						stop: $scope.scroller.apps.stop
					});
			}

			$('#queuing').on('counterStop', function() {
				queuingCounter('0.4', true);
			});

			$('#ticketing').on('counterStop', function() {
				ticketSoldCounter('500', true);
			});

			$('#apps').on('counterStop', function() {
				mobileAppsCounter('10000', true);
			});

			$scope.restart = function (reset){
				queuingCounter('0.4', reset);
				ticketSoldCounter('500', reset);
				mobileAppsCounter('10000', reset);
			}



		}]
)